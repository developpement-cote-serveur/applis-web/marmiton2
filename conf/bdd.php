<?php

// Connexion à MySQL et sélection de la base de données
$mysqli = new mysqli('localhost', 'root', '', 'marmiton2');
$mysqli->set_charset("utf8");

// Gestion des erreurs
if ($mysqli->connect_errno) {
    echo "Navré mais il y a eu un léger souci avec la connexion à la base de données.";

    echo "Error: Messages MySQL : \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";

    echo "Le serveur MySQL est-il démarré ? Les informations de connexion sont-elles correctes (allez voir dans le fichier conf/bdd.php) ? Avez-vous créé la base de données avec le bon nom ?";

    exit;
}

?>
