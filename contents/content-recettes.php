<section id="recettes" class="container">
    <div class="search">
        <h2>Rechercher une recette</h2>
        <form class="f_search" action="recettes.php" method="get">
            <input 
                type="text"
                name="search"
                placeholder="Entrez un mot-clé"
                value="<?php echo (isset($_GET['search']) ? $_GET['search'] : ''); ?>">
            <input type="submit" value="Rechercher" class="button icon fa-search">
        </form>
    </div>
<?php

$search = (isset($_GET['search']) ? $_GET['search'] : null);

$recettes = getRecettes($mysqli, null, $search);

if (count($recettes) > 0) {
?>
    <div class="box alt">
        <div class="teasers-wrapper row 50% uniform">
            <?php
            // Itération sur les 3 autres recettes récupérées
            foreach ($recettes as $recette) {
            ?>
                <article class="teaser 4u">
                    <a href="recette.php?id=<?php echo $recette['id']; ?>"
                       title="Accéder à la recette &quote;<?php echo $recette['R_intitule'] ?>&quote;">
                        <header>
                            <h2><?php echo $recette['R_intitule']; ?></h2>
                            <span class="image fit">
                                <img src="<?php echo $recette['photo']; ?>" alt="" />
                            </span>
                        </header>

                        <div class="content">
                            <?php echo $recette['description']; ?>
                        </div>

                        <footer>
                            <span class="tag"><?php echo $recette['C_intitule']; ?></span>
                        </footer>
                    </a>
                </article>
            <?php
            }
            ?>

        </div>
    </div>

    <?php
    } else {
        echo 'Aucune recette trouvée :(';
    }
    ?>

</section>
