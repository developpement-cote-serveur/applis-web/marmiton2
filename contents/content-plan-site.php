<!-- Affichage du plan du site -->
<h1>Déménagement, Bernos-Beaulac (33) : plan du site</h1>
<ul id="ideo_inc_planSite">
    <li>
        <a href='index.php' title=''>Accueil</a>

    </li>
    <li>
        <a href='demenagement.php' title=''>Déménagement</a>

    </li>
    <li>
        <a href='transfert.php' title=''>Transfert</a>

    </li>
    <li>
        <a href='garde-meubles.php' title=''>Garde-meubles</a>

    </li>
    <li>
        <a href='mentions-legales.php' title=''>Mentions légales</a>

    </li>
    <li>
        <a href='plan-site.php' title=''>Plan du site</a>

    </li>
</ul>
