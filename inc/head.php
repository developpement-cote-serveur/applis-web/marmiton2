<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  lang="fr">
<head>
    <title>Marmiton - Le VRAI !</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta lang="fr" name="keywords" content="manger, manger, manger" />
    <meta lang="fr" name="description" content="Parce que manger c'est la vie, venez vivre avec nous ici ! ;)" />

    <link type="text/css" href="assets/css/animate.css" rel="stylesheet" />
    <link type="text/css" href="assets/css/main.css" rel="stylesheet" />
    <link type="text/css" href="assets/css/styles.css" rel="stylesheet" />
    <link type="text/css" href="assets/lib/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
    <link type="text/css" href="assets/lib/select2/select2.min.css" rel="stylesheet" />
    <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>    
</head>

<?php
    include 'conf/bdd.php';
    include 'inc/functions.php';
?>
