$(document).ready(function() {
    function addImage(item) {
        if (!item.id) {
            return item.text;
        }

        var $item = $(
            '<span><img src="' + item.element.dataset.image + '" alt="" /><span class="intitule">' + item.text + '</span></span>'
        );

        return $item;
    }

    function updateWeight(target, element) {
        var i = 1;
        $(target).find(element).each(function() {
            $(this).find('.weight').html(i);
            i++;
        });
    }

    $('.select2').select2({
        templateResult: addImage
    });

    $('.sortable').sortable({
        handle: ".handle",
        placeholder: "ui-state-highlight",
        forcePlaceholderSize: true,
        update: function( event, ui ) {
            updateWeight($(this), 'li');
        }
    }).disableSelection();

    /**
     * Ajout d'un élément au clic d'un bouton avec la class "add-item"
     */
    $('.add-item').click(function(e) {
        e.preventDefault();

        var target = $('#' + $(this).attr('data-target'));
        var element = $(target).find(':first').clone();
        
        $(element).find('textarea').html('');
        $(target).append($(element));
        updateWeight($(target), 'li');
        initRemoveButtons();        
    });

    function initRemoveButtons() {
        $('.remove-item').off().click(function(e) {
            e.preventDefault();
    
            var parent = $('#' + $(this).attr('data-parent'));

            if ($(parent).find('li').length < 2) {
                alert('Vous devez avoir au moins une étape !');
                return;
            }

            $(this).closest('li').remove();
            updateWeight($(parent), 'li');
        });
    }

    initRemoveButtons();
});
